# Introduction

This module exists only to serve as a minimal test case for
[kdesrc-build](https://invent.kde.org/sdk/kdesrc-build/).

* You can configure this module with CMake (pass `-DDO_SUCCEED:BOOL=1` to `cmake`)
    * Or force it to fail to configure
* Once configured, you can run an empty build
    * Or force the build to fail (build the `fail_to_build` target)
* A fake install target is also provided which fakes a successful install
    * If you want the install to fail, pass `-DDO_INSTALL:BOOL=0` to `cmake`. You will
      probably want to also pass `-DDO_SUCCEED:BOOL=1` so your fake build target
      completes successfully.

# License

The work is trivial, nonetheless it is licensed under the same terms as
kdesrc-build itself, GNU GPL v2.
